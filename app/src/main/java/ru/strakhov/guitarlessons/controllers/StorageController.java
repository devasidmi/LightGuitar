package ru.strakhov.guitarlessons.controllers;

import android.content.Context;

import com.snatik.storage.Storage;

import ru.strakhov.guitarlessons.R;

public class StorageController {

    private final Storage mStorage;
    private final String path;
    private final String sessionFilename;

    public StorageController(Context context) {
        sessionFilename = context.getResources().getString(R.string.session_status_filename);
        path = context.getFilesDir().getPath();
        this.mStorage = new Storage(context);
    }

    public void saveSession(String userId) {
        mStorage.createFile(path + sessionFilename, userId);
    }

    public String getUserId() {
        return mStorage.readTextFile(path + sessionFilename);
    }

    public boolean checkIsActiveSession() {
        return mStorage.getFile(path + sessionFilename).exists();
    }

    public void logout() {
        if (checkFileExists(sessionFilename)) {
            mStorage.deleteFile(path + sessionFilename);
        }
    }

    private boolean checkFileExists(String filename) {
        return mStorage.getFile(path + filename).exists();
    }
}
