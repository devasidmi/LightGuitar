package ru.strakhov.guitarlessons.controllers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.GlideRequests;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.activities.AuthActivity;
import ru.strakhov.guitarlessons.activities.MainActivity;
import ru.strakhov.guitarlessons.models.User;

public class NavigationDrawerController implements Drawer.OnDrawerItemClickListener {
    private final Activity mActivity;
    private final Toolbar mToolbar;
    private final Context mContext;
    private ArrayList<IDrawerItem> items;
    private static final String CATALOG = "catalog";
    private static final String CHAT = "chat";
    private static final String SOCIAL = "social";
    private static final String LICENSE = "license";
    private static final String LOGOUT = "logout";
    private static final String RATE = "rate";
    private FirebaseStorage mFirebaseStorage;
    private FirebaseFirestore mDatabase;

    public NavigationDrawerController(Activity activity, Toolbar toolbar, Context context) {
        this.mActivity = activity;
        this.mToolbar = toolbar;
        this.mContext = context;
        setupStorageDatabase();
    }

    private void setupStorageDatabase() {
        mFirebaseStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseFirestore.getInstance();
    }

    private void initItems() {
        items = new ArrayList<>();
        String catalog, chat, social, rate, license, logout;

        catalog = mContext.getResources().getString(R.string.catalog_item);
        chat = mContext.getResources().getString(R.string.chat_item);
        social = mContext.getResources().getString(R.string.social_item);
        rate = mContext.getResources().getString(R.string.rate_item);
        license = mContext.getResources().getString(R.string.license_item);
        logout = mContext.getResources().getString(R.string.logout_item);

        items.add(new PrimaryDrawerItem()
                .withName(catalog).withTag("catalog").withIcon(R.drawable.study_24));
        items.add(new PrimaryDrawerItem()
                .withName(chat).withTag("chat").withIcon(R.drawable.chat_room_24));
        items.add(new PrimaryDrawerItem()
                .withName(social).withTag("social").withIcon(R.drawable.user_account_24));
        items.add(new DividerDrawerItem());
        items.add(new PrimaryDrawerItem()
                .withName(rate).withTag("rate").withIcon(R.drawable.google_play_24).withSelectable(false));
        items.add(new PrimaryDrawerItem()
                .withName(license).withTag("license").withIcon(R.drawable.copyright_24).withSelectable(false));
        items.add(new DividerDrawerItem());
        items.add(new PrimaryDrawerItem()
                .withName(logout).withTag("logout").withIcon(R.drawable.logout_24).withSelectable(false));
    }

    @Nullable
    @BindView(R.id.profile_image)
    AppCompatImageView mProfileImage;
    @Nullable
    @BindView(R.id.user_fullname)
    AppCompatTextView mUserFullnameText;
    @Nullable
    @BindView(R.id.author_image)
    AppCompatImageView mAuthorImage;

    private void loadImage(Uri image) {
        GlideRequests mGlideApp = GlideApp.with(mContext);
        if (image == null && mProfileImage != null) {
            mGlideApp.load(R.drawable.empty_user_avatar).centerCrop().circleCrop().into(mProfileImage);
        } else {
            if (mProfileImage != null) {
                mGlideApp.load(image).centerCrop().circleCrop().into(mProfileImage);
            }
        }
    }

    private View initHeader() {
        @SuppressLint("InflateParams") View v = LayoutInflater.from(mContext).inflate(R.layout.navigation_header, null, false);
        ButterKnife.bind(this, v);
        final String userId = new StorageController(mContext).getUserId();
        StorageReference mUserImageReference = mFirebaseStorage.getReference().child(String.format("users_images/%s.jpg", userId));
        mUserImageReference.getDownloadUrl().addOnSuccessListener(mActivity, new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                loadImage(uri);
            }
        }).addOnFailureListener(mActivity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                loadImage(null);
            }
        });
        mDatabase.collection("users").document(String.format("%s", userId)).get().addOnSuccessListener(mActivity, new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                try {
                    User user = documentSnapshot.toObject(User.class);
                    if (mUserFullnameText != null) {
                        mUserFullnameText.setText(user.getFullname());
                    }
                } catch (Exception ex) {
                    Toast.makeText(mContext, "Ошибка дступа", Toast.LENGTH_LONG).show();
                }
            }
        }).addOnFailureListener(mActivity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Log.e("YAY!", "Can't get user info");
            }
        });
        return v;
    }

    public void create() {
        initItems();
        new DrawerBuilder()
                .withActivity(mActivity)
                .withToolbar(mToolbar)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withHeader(initHeader())
                .withDrawerItems(items)
                .withOnDrawerItemClickListener(this)
                .withDelayDrawerClickEvent(250)
                .withCloseOnClick(true)
                .build();
    }

    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        int title = -1;
        final Intent mScreenIntent = new Intent();
        mScreenIntent.setAction(MainActivity.CHANGE_SCREEN);
        switch (drawerItem.getTag().toString()) {
            case CATALOG:
                title = R.string.catalog;
                mScreenIntent.putExtra("screen", MainActivity.LESSONS_SCREEN);
                break;
            case CHAT:
                title = R.string.chat;
                mScreenIntent.putExtra("screen", MainActivity.CHAT_SCREEN);
                break;
            case SOCIAL:
                title = R.string.social;
                mScreenIntent.putExtra("screen", MainActivity.SOCIAL_SCREEN);
                break;
            case RATE:
                String mPackageId = mContext.getResources().getString(R.string.package_id);
                Intent mAppPageIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(String.format("http://play.google.com/store/apps/details?id=%s", mPackageId)));
                mContext.startActivity(mAppPageIntent);
                break;
            case LICENSE:
                String mLicenseTitle = mContext.getResources().getString(R.string.license_title);
                @SuppressLint("InflateParams") View mLicenseView = LayoutInflater.from(mContext).inflate(R.layout.license_dialog, null, false);
                ButterKnife.bind(this, mLicenseView);
                if (mAuthorImage != null) {
                    GlideApp.with(mContext)
                            .load(R.drawable.author)
                            .centerCrop()
                            .circleCrop()
                            .into(mAuthorImage);
                }
                MaterialDialog mdb = new MaterialDialog.Builder(mContext)
                        .title(mLicenseTitle)
                        .customView(mLicenseView, false)
                        .build();
                mdb.show();
                break;
            case LOGOUT:
                new StorageController(mContext).logout();
                mContext.startActivity(new Intent(mContext, AuthActivity.class));
                mActivity.finish();
                break;
        }
        if (title != -1) {
            Intent mTitleIntent = new Intent();
            mTitleIntent.putExtra("title", title);
            mTitleIntent.setAction(MainActivity.CHANGE_TITLE);
            mContext.sendBroadcast(mTitleIntent);
            mContext.sendBroadcast(mScreenIntent);
        }
        return true;
    }
}
