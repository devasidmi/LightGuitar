package ru.strakhov.guitarlessons;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import ru.strakhov.guitarlessons.activities.MainActivity;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        showAd(remoteMessage);
    }

    private void showAd(RemoteMessage remoteMessage) {
        Intent mIntent = new Intent();
        Bundle mAdBundle = new Bundle();
        Map<String, String> rmData = remoteMessage.getData();
        mAdBundle.putString("title", rmData.get("title"));
        mAdBundle.putString("imgUrl", rmData.get("imgUrl"));
        mAdBundle.putString("content", rmData.get("content"));
        mAdBundle.putString("link", rmData.get("link"));
        mIntent.putExtra("adData", mAdBundle);
        mIntent.setAction(MainActivity.SHOW_AD);
        this.sendBroadcast(mIntent);
    }
}
