package ru.strakhov.guitarlessons.models;

import com.google.firebase.firestore.DocumentReference;

import java.util.Date;

public class Message {

    private DocumentReference from;
    private String message;
    private Date when;

    public Message() {
    }

    public Message(DocumentReference from, String message, Date when) {
        this.from = from;
        this.message = message;
        this.when = when;
    }

    public DocumentReference getFrom() {
        return from;
    }

    public void setFrom(DocumentReference from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }
}
