package ru.strakhov.guitarlessons.models;

public class User {

    private String email;
    private String fullname;
    private String password;
    private int openLessons;

    public User() {
    }

    public User(String email, String fullname, String password, int openLessons) {
        this.email = email;
        this.fullname = fullname;
        this.password = password;
        this.openLessons = openLessons;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getOpenLessons() {
        return openLessons;
    }

    public void setOpenLessons(int openLessons) {
        this.openLessons = openLessons;
    }
}
