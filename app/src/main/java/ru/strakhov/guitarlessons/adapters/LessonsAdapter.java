package ru.strakhov.guitarlessons.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.activities.LessonActivity;
import ru.strakhov.guitarlessons.models.Lesson;

public class LessonsAdapter extends RecyclerView.Adapter<LessonsAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Lesson> lessons;
    private final int openLessonsCount;

    public LessonsAdapter(Context mContext, List<Lesson> lessons, int openLessonsCount) {
        this.mContext = mContext;
        this.lessons = lessons;
        this.openLessonsCount = openLessonsCount;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.lesson_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (!(position + 1 <= openLessonsCount)) {
            holder.lock_image.setVisibility(View.VISIBLE);
        } else {
            holder.lock_image.setVisibility(View.GONE);
        }
        holder.lessonTitle.setText(lessons.get(position).getTitle());
        holder.lessonSummary.setText(lessons.get(position).getSummary());
        holder.lessonNumber.setText(String.valueOf(position + 1));
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.lesson_title)
        AppCompatTextView lessonTitle;
        @BindView(R.id.lesson_summary)
        AppCompatTextView lessonSummary;
        @BindView(R.id.lesson_number)
        AppCompatTextView lessonNumber;
        @BindView(R.id.lock_status_image)
        AppCompatImageView lock_image;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() + 1 <= openLessonsCount) {
                Intent mIntent = new Intent(mContext, LessonActivity.class);
                mIntent.putExtra("lessonNumber", getAdapterPosition() + 1);
                mIntent.putExtra("title", lessons.get(getAdapterPosition()).getTitle());
                mIntent.putExtra("startPage", lessons.get(getAdapterPosition()).getStartPage());
                mIntent.putExtra("endPage", lessons.get(getAdapterPosition()).getEndPage());
                mIntent.putExtra("youtubeUrl", lessons.get(getAdapterPosition()).getYoutubeUrl());
                mIntent.putExtra("lessonsOpen", openLessonsCount);
                mContext.startActivity(mIntent);
            }
        }
    }
}
