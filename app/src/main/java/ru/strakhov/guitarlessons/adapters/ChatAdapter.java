package ru.strakhov.guitarlessons.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.models.Message;
import ru.strakhov.guitarlessons.models.User;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private final Context mContext;
    private final ArrayList<Message> messages;
    private final StorageReference mFireStorage;

    public ChatAdapter(Context mContext, ArrayList<Message> messages) {
        this.mContext = mContext;
        this.messages = messages;
        mFireStorage = FirebaseStorage.getInstance().getReference();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_bubble, parent, false));
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final String userId = messages.get(position).getFrom().getId();
        Date sendDate = messages.get(position).getWhen();
        holder.mUserImage.setVisibility(View.INVISIBLE);
        holder.mMessageText.setText(messages.get(position).getMessage());
        if (sendDate.getMinutes() < 10) {
            holder.mSendTimeText.setText(String.format("%d:0%d", sendDate.getHours(), sendDate.getMinutes()));
        } else {
            holder.mSendTimeText.setText(String.format("%d:%d", sendDate.getHours(), sendDate.getMinutes()));
        }

        mFireStorage.child(String.format("users_images/%s.jpg", userId)).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                GlideApp.with(mContext)
                        .load(uri)
                        .centerCrop()
                        .circleCrop()
                        .error(R.drawable.empty_user_avatar)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.mUserImage.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.mUserImage.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(holder.mUserImage);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                GlideApp.with(mContext)
                        .load(R.drawable.empty_user_avatar)
                        .centerCrop()
                        .circleCrop()
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                holder.mUserImage.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.mUserImage.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(holder.mUserImage);
            }
        });
        messages.get(position).getFrom().get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user = documentSnapshot.toObject(User.class);
                holder.mSendUserText.setText(user.getFullname());
            }
        });
    }

    public void clearMessages() {
        this.messages.clear();
    }

    public void addMessage(Message message) {
        this.messages.add(message);
        notifyItemInserted(messages.size() - 1);
    }

    public void deleteMessage(Message message) {
        int cursor = 0;
        for (Message msg : messages) {
            if (msg.getMessage().equals(message.getMessage()) &&
                    msg.getFrom().equals(message.getFrom()) &&
                    msg.getWhen().equals(message.getWhen())) {
                this.messages.remove(cursor);
                notifyItemRemoved(cursor);
                return;
            }
            cursor++;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        AppCompatImageView mUserImage;
        @BindView(R.id.message)
        AppCompatTextView mMessageText;
        @BindView(R.id.send_user)
        AppCompatTextView mSendUserText;
        @BindView(R.id.send_time)
        AppCompatTextView mSendTimeText;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}

