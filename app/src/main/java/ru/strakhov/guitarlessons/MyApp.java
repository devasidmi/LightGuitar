package ru.strakhov.guitarlessons;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Context mContext = getApplicationContext();
        MobileAds.initialize(mContext, mContext.getResources().getString(R.string.admob_appID));
        FirebaseMessaging.getInstance().subscribeToTopic("ads");
        FirebaseInstanceId.getInstance().getToken();
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
    }
}
