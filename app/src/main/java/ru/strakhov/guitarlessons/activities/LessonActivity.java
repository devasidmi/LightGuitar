package ru.strakhov.guitarlessons.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.controllers.StorageController;
import ru.strakhov.guitarlessons.models.User;

public class LessonActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.pdfView)
    PDFView mPdfView;
    @BindView(R.id.adView)
    AdView mAdView;
    private String title = "", youtubeUrl = "";
    private int start = -1, end = -1, lessonNumer = -1, lessonsOpen = -1;
    private Context mContext;
    private StorageController mStorageController;
    private FirebaseFirestore mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        mContext = this;
        mDatabase = FirebaseFirestore.getInstance();
        mStorageController = new StorageController(mContext);
        ButterKnife.bind(this);
        loadAd();
        setupToolbar();
        readBundle();
        changeTitle(title);
        openLesson();
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void readBundle() {
        Intent args = getIntent();
        Bundle mBundle = args.getExtras();
        if (mBundle != null) {
            lessonNumer = (int) args.getExtras().get("lessonNumber");
            title = (String) args.getExtras().get("title");
            start = (int) args.getExtras().get("startPage") - 1;
            end = (int) args.getExtras().get("endPage") - 1;
            youtubeUrl = (String) args.getExtras().get("youtubeUrl");
            lessonsOpen = (int) args.getExtras().get("lessonsOpen");
        }
    }

    private void changeTitle(String title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }

    private int[] getPages() {
        List<Integer> pages = new ArrayList<>();
        for (int i = start; i <= end; ++i) {
            pages.add(i);
        }
        int pageRange[] = new int[pages.size()];
        for (int k = 0; k < pages.size(); ++k) {
            pageRange[k] = pages.get(k);
        }
        return pageRange;
    }

    private void openLesson() {
        AssetManager assetManager = getAssets();
        try {
            InputStream ims = assetManager.open(this.getString(R.string.pdf_file));
            mPdfView
                    .fromStream(ims)
                    .pages(getPages())
                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .load();
            mPdfView.fitToWidth();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void unlockLesson() {
        final String userId = mStorageController.getUserId();
        mDatabase.collection("users").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user = documentSnapshot.toObject(User.class);
                mDatabase.collection("users").document(userId).update("openLessons", user.getOpenLessons() + 1).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        lessonsOpen++;
                        invalidateOptionsMenu();
                        Toast.makeText(mContext, mContext.getResources().getString(R.string.unlock_lesson_message), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.lesson_menu, menu);
        final MenuItem youtubeItem = menu.findItem(R.id.youtube_menu_item);
        final MenuItem doneItem = menu.findItem(R.id.done_menu_item);
        youtubeItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                youtubeItem.setEnabled(false);
                if (!(lessonNumer + 1 <= lessonsOpen)) {
                    unlockLesson();
                }
                Intent mYoutubeVideoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeUrl));
                String mYoutubePackageId = mContext.getResources().getString(R.string.YOUTUBE_PACKAGE_ID);
                if (mContext.getPackageManager().getLaunchIntentForPackage(mYoutubePackageId) != null)
                    mYoutubeVideoIntent.setPackage(mYoutubePackageId);
                mContext.startActivity(mYoutubeVideoIntent);
                return true;
            }
        });
        doneItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                doneItem.setEnabled(false);
                unlockLesson();
                return true;
            }
        });
        if (youtubeUrl.isEmpty()) {
            youtubeItem.setVisible(false);
            if (!(lessonNumer + 1 <= lessonsOpen))
                doneItem.setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }
}
