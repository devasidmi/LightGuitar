package ru.strakhov.guitarlessons.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.controllers.NavigationDrawerController;
import ru.strakhov.guitarlessons.fragments.ChatFragment;
import ru.strakhov.guitarlessons.fragments.LessonsFragment;
import ru.strakhov.guitarlessons.fragments.SocialFragment;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    //ACTIONS
    public static final String CHANGE_TITLE = "changeTitle";
    public static final String CHANGE_SCREEN = "changeScreen";
    public static final String SHOW_AD = "showAd";

    // SCREENS
    public static final String LESSONS_SCREEN = "lessonsScreen";
    public static final String SOCIAL_SCREEN = "socialScreen";
    public static final String CHAT_SCREEN = "chatScreen";

    private BroadcastReceiver mListener;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        NavigationDrawerController mNavigationDrawer = new NavigationDrawerController(this, mToolbar, this);
        mNavigationDrawer.create();
        setupListener();
        openStartScreen();
    }

    private void openStartScreen() {
        changeToolbarTitle(R.string.catalog);
        FragmentManager mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.fragment_container, new LessonsFragment(), LessonsFragment.TAG).commit();
    }

    private void changeScreen(String screen) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        switch (screen) {
            case LESSONS_SCREEN:
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof LessonsFragment)) {
                    mFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, new LessonsFragment(), LessonsFragment.TAG)
                            .commit();
                }
                break;
            case SOCIAL_SCREEN:
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof SocialFragment)) {
                    mFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, new SocialFragment(), SocialFragment.TAG)
                            .commit();
                }
                break;
            case CHAT_SCREEN:
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof ChatFragment)) {
                    mFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, new ChatFragment(), ChatFragment.TAG)
                            .commit();
                }
                break;
        }
    }

    private void setupListener() {
        mListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle args = intent.getExtras();
                String action = intent.getAction();
                if (action != null) {
                    switch (action) {
                        case CHANGE_TITLE:
                            if (args != null) {
                                Integer title = (Integer) args.get("title");
                                if (title != null)
                                    changeToolbarTitle(title);
                            }
                            break;
                        case CHANGE_SCREEN:
                            if (args != null) {
                                String screen = (String) args.get("screen");
                                if (screen != null)
                                    changeScreen(screen);
                            }
                            break;
                        case SHOW_AD:
                            final Bundle adBundle = args.getBundle("adData");
                            View adView = LayoutInflater.from(mContext).inflate(R.layout.dialog_ad, null, false);

                            AppCompatImageView mAdImage = adView.findViewById(R.id.adsImage);
                            AppCompatTextView mAdContent = adView.findViewById(R.id.ad_content);
                            final ProgressBar mAdLoader = adView.findViewById(R.id.ad_loading_progress);

                            GlideApp.with(mContext).load(adBundle.getString("imgUrl")).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    mAdLoader.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(mAdImage);
                            mAdContent.setText(adBundle.getString("content"));

                            MaterialDialog md = new MaterialDialog.Builder(mContext)
                                    .customView(adView, true)
                                    .positiveText("Подробнее")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent i = new Intent(Intent.ACTION_VIEW);
                                            i.setData(Uri.parse(adBundle.getString("link")));
                                            startActivity(i);
                                        }
                                    })
                                    .cancelable(false)
                                    .build();
                            md.show();
                            break;
                    }
                }

            }
        };
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(CHANGE_TITLE);
        mIntentFilter.addAction(CHANGE_SCREEN);
        mIntentFilter.addAction(SHOW_AD);
        this.registerReceiver(mListener, mIntentFilter);
    }

    private void changeToolbarTitle(int title) {
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mListener);
        mListener = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mListener == null)
            setupListener();
    }
}
