package ru.strakhov.guitarlessons.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.R;

public class AdsActivity extends AppCompatActivity {

    @BindView(R.id.adsImage)
    AppCompatImageView mAdImage;
    @BindView(R.id.ad_content)
    AppCompatTextView mAdContent;
    @BindView(R.id.ad_loading_progress)
    ProgressBar mAdLoader;

    String imgUrl, content, link;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);
        mContext = this;
        ButterKnife.bind(this);
        Bundle mBundle = this.getIntent().getExtras();
        if (mBundle != null) {
            imgUrl = (String) mBundle.get("imgUrl");
            content = (String) mBundle.get("content");
            link = (String) mBundle.get("link");
            setupUI();
        }
    }

    private void setupUI() {
        mAdContent.setText(content);
        GlideApp.with(mContext).
                load(imgUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                mAdLoader.setVisibility(View.GONE);
                return false;
            }
        }).into(mAdImage);

    }

    @OnClick(R.id.adInfo)
    void onAdInfoClick() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        startActivity(i);
    }
}
