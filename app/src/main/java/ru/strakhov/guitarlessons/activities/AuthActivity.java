package ru.strakhov.guitarlessons.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.fragments.AuthFragment;
import ru.strakhov.guitarlessons.fragments.RegisterFragment;

public class AuthActivity extends AppCompatActivity {

    public static final String OPEN_REGISTER = "openRegister";
    public static final String INVALIDATE = "invalidate";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private FragmentManager mFragmentManager;
    private Context mContext;
    private BroadcastReceiver mAuthBroadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mContext = this;
        mFragmentManager = getSupportFragmentManager();
        ButterKnife.bind(this);
        setupUI();
    }

    private void loadAuthFragment() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new AuthFragment(), AuthFragment.TAG)
                .commit();
    }

    private void setupUI() {
        setSupportActionBar(mToolbar);
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null)
            getSupportActionBar().setTitle(R.string.auth);
        loadAuthFragment();
    }

    private void openRegister() {
        ActionBar mActionBar = getSupportActionBar();
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new RegisterFragment(), RegisterFragment.TAG)
                .commit();
        if (mActionBar != null) {
            mActionBar.setTitle(this.getResources().getString(R.string.register));
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initListener() {
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(OPEN_REGISTER);
        mIntentFilter.addAction(INVALIDATE);
        mAuthBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null) {
                    switch (action) {
                        case OPEN_REGISTER:
                            openRegister();
                            break;
                        case INVALIDATE:
                            invalidateOptionsMenu();
                            break;
                    }
                }

            }
        };
        mContext.registerReceiver(mAuthBroadcastReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent mStopAuthIntent = new Intent();
        mStopAuthIntent.setAction(AuthFragment.STOP);
        this.sendBroadcast(mStopAuthIntent);
        this.unregisterReceiver(mAuthBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.auth_menu, menu);
        MenuItem mAuthBtn = menu.findItem(R.id.auth_menu_login_btn);
        if (mFragmentManager.findFragmentById(R.id.fragment_container) instanceof AuthFragment) {
            mAuthBtn.setVisible(true);
            mAuthBtn.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    Intent mAuthIntent = new Intent();
                    mAuthIntent.setAction(AuthFragment.AUTH);
                    mContext.sendBroadcast(mAuthIntent);
                    return true;
                }
            });
        } else {
            mAuthBtn.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void navigateBack() {
        ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mActionBar.setTitle(this.getResources().getString(R.string.auth));
        }
        loadAuthFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                navigateBack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager.findFragmentById(R.id.fragment_container) instanceof RegisterFragment) {
            navigateBack();
        } else {
            super.onBackPressed();
        }

    }
}
