package ru.strakhov.guitarlessons.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.adapters.ChatAdapter;
import ru.strakhov.guitarlessons.controllers.KeyBoardController;
import ru.strakhov.guitarlessons.controllers.StorageController;
import ru.strakhov.guitarlessons.models.Message;

public class ChatFragment extends Fragment implements View.OnFocusChangeListener {

    public static final String TAG = "ChatFragment";
    @BindView(R.id.message_input)
    MaterialEditText mMessageInput;
    @BindView(R.id.chat_list)
    RecyclerView mChatList;

    private ChatAdapter mChatAdapter;
    private Context mContext;
    private FirebaseFirestore mDatabase;
    private KeyBoardController mKeyBoardController;
    private View mView;
    private ListenerRegistration newMesagesListener;
    private DocumentReference userRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_chat, container, false);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        init();
        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        mContext = getContext();
        mKeyBoardController = new KeyBoardController(mContext);
        ButterKnife.bind(this, mView);
        mMessageInput.setOnFocusChangeListener(this);
        setupDatabase();
        getUserRef();
        mChatAdapter = new ChatAdapter(mContext, new ArrayList<Message>());
        mChatList.setLayoutManager(new LinearLayoutManager(mContext));
        mChatList.setAdapter(mChatAdapter);
    }

    private void getUserRef() {
        String userId = new StorageController(mContext).getUserId();
        userRef = mDatabase.document(String.format("users/%s", userId));
    }

    private void setupDatabase() {
        mDatabase = FirebaseFirestore.getInstance();
    }

    private void listenNewMessages() {
        newMesagesListener = mDatabase.collection("chat")
                .orderBy("when", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (e == null) {
                            for (DocumentChange change : documentSnapshots.getDocumentChanges()) {
                                switch (change.getType()) {
                                    case ADDED:
                                        mChatAdapter.addMessage(change.getDocument().toObject(Message.class));
                                        mChatList.scrollToPosition(mChatAdapter.getItemCount() - 1);
                                        break;
                                    case REMOVED:
                                        mChatAdapter.deleteMessage(change.getDocument().toObject(Message.class));
                                        break;
                                }
                            }
                        }
                    }
                });
    }

    @OnClick(R.id.send_message_button)
    void onSendMessageClick() {
        String messageText = mMessageInput.getText().toString().trim();
        if (!messageText.isEmpty() && userRef != null) {
            Message message = new Message();
            message.setFrom(userRef);
            message.setMessage(messageText.trim());
            message.setWhen(Calendar.getInstance().getTime());
            mDatabase.collection("chat").add(message).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    mMessageInput.setText("");
                    mKeyBoardController.hideKeyboard(mMessageInput);
                    mChatList.scrollToPosition(mChatAdapter.getItemCount() - 1);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
//                    Log.e("YAY!", "send message error");
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
//        Log.e("YAY!", "stop listener");
        mChatAdapter.clearMessages();
        newMesagesListener.remove();
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatList.setAdapter(mChatAdapter);
        listenNewMessages();
    }

    @Override
    public void onFocusChange(View view, boolean isFocus) {
        if (isFocus)
            mChatList.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }
}
