package ru.strakhov.guitarlessons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.adapters.LessonsAdapter;
import ru.strakhov.guitarlessons.controllers.StorageController;
import ru.strakhov.guitarlessons.models.Lesson;
import ru.strakhov.guitarlessons.models.User;

public class LessonsFragment extends Fragment {

    public static final String TAG = "LessonsFragment";
    private Context mContext;
    @BindView(R.id.lessons_list)
    RecyclerView lessonsList;
    @BindView(R.id.adView)
    AdView mAdView;
    private FirebaseFirestore mDatabase;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_lessons, container, false);
        ButterKnife.bind(this, mView);
        loadAdd();
        mContext = getContext();
        mDatabase = FirebaseFirestore.getInstance();
        return mView;
    }

    private void loadAdd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private List<Lesson> readLessonsFile() {
        List<Lesson> lessons = new ArrayList<>();
        try {
            InputStream jsonFile = mContext.getAssets().open(mContext.getResources().getString(R.string.lessons_json_file));
            StringBuilder buf = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(jsonFile, "UTF-8"));
            String str;
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
            lessons = Arrays.asList(new GsonBuilder().create().fromJson(buf.toString(), Lesson[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    private void getUserOpenLessons(final List<Lesson> lessons) {
        String userId = new StorageController(mContext).getUserId();
        mDatabase.collection("users").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                try {
                    User mUser = documentSnapshot.toObject(User.class);
                    int openLessonsCount = mUser.getOpenLessons();
                    LessonsAdapter mLessonsAdapter = new LessonsAdapter(mContext, lessons, openLessonsCount);
                    lessonsList.setLayoutManager(new LinearLayoutManager(mContext));
                    lessonsList.setAdapter(mLessonsAdapter);
                    if (openLessonsCount != 9)
                        lessonsList.scrollToPosition(openLessonsCount - 1);
                } catch (Exception err) {
                    Toast.makeText(mContext,"Ошибка доступа",Toast.LENGTH_LONG).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mContext, "Ошибка доступа", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setupLessons() {
        List<Lesson> lessons = readLessonsFile();
        getUserOpenLessons(lessons);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mContext != null) {
            setupLessons();
        }
    }
}
