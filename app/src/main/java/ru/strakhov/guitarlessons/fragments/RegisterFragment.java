package ru.strakhov.guitarlessons.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.pvryan.easycrypt.ECResultListener;
import com.pvryan.easycrypt.hash.ECHash;
import com.pvryan.easycrypt.hash.ECHashAlgorithms;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.activities.AuthActivity;
import ru.strakhov.guitarlessons.activities.MainActivity;
import ru.strakhov.guitarlessons.controllers.StorageController;

public class RegisterFragment extends Fragment {

    public static final String TAG = "RegisterFragment";
    @BindView(R.id.email_input)
    MaterialEditText emailInput;
    @BindView(R.id.fullname_input)
    MaterialEditText fullnameInput;
    @BindView(R.id.password_input)
    MaterialEditText passwordInput;
    @BindView(R.id.password_submit_input)
    MaterialEditText passwordSubmitInput;
    @BindView(R.id.profile_image)
    AppCompatImageView profileImage;
    private Context mContext;
    private FirebaseFirestore mDatabase;
    private Uri mProfileImage;
    private MaterialDialog mRegisterDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_register, container, false);
        mContext = getContext();
        ButterKnife.bind(this, mView);
        if (mContext != null)
            mContext.sendBroadcast(new Intent().setAction(AuthActivity.INVALIDATE));
        setupUI();
        setupDatabase();
        return mView;
    }

    private void setupDatabase() {
        FirebaseFirestoreSettings mFirebaseFirestoreSettings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        mDatabase = FirebaseFirestore.getInstance();
        mDatabase.setFirestoreSettings(mFirebaseFirestoreSettings);
    }

    private void setupUI() {
        loadImage(mContext.getResources().getDrawable(R.drawable.empty_user_avatar));
    }

    private void loadImage(Drawable image) {
        GlideApp.with(mContext)
                .load(image)
                .centerCrop()
                .circleCrop()
                .into(profileImage);
    }

    private void loadImage(Uri image) {
        GlideApp.with(mContext)
                .load(image)
                .centerCrop()
                .circleCrop()
                .into(profileImage);
    }

    private boolean checkInputs() {
        boolean isValid = true;
        if (emailInput.getText().toString().trim().isEmpty()) {
            emailInput.setError(mContext.getResources().getString(R.string.error_message_input));
            isValid = false;
        }

        if (fullnameInput.getText().toString().trim().isEmpty()) {
            fullnameInput.setError(mContext.getResources().getString(R.string.error_message_input));
            isValid = false;
        }

        if (passwordInput.getText().toString().trim().isEmpty()) {
            passwordInput.setError(mContext.getResources().getString(R.string.error_message_input));
            isValid = false;
        }
        if (passwordSubmitInput.getText().toString().trim().isEmpty()) {
            passwordSubmitInput.setError(mContext.getResources().getString(R.string.error_message_input));
            isValid = false;
        }
        if (!(passwordInput.getText().toString().trim().equals(passwordSubmitInput.getText().toString().trim()))) {
            passwordSubmitInput.setError(mContext.getResources().getString(R.string.error_password_match));
            isValid = false;
        }
        return isValid;
    }

    private void checkDuplicateUser(String email, final Activity activity) {
        mRegisterDialog = initRegisterDialog();
        mRegisterDialog.show();
        final CollectionReference usersCollectionReference = mDatabase.collection("users");
        usersCollectionReference.whereEqualTo("email", email)
                .get().addOnSuccessListener(activity, new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                int usersCount = documentSnapshots.getDocuments().size();
                if (usersCount > 0) {
                    mRegisterDialog.dismiss();
                    Toast.makeText(mContext, "Пользователь с такой почтой уже существует", Toast.LENGTH_LONG).show();
                } else {
                    createUser(activity);
                }
            }
        }).addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Log.e("YAY!", "checkDuplicate error");
            }
        });
    }

    @OnClick(R.id.register_btn)
    void onRegisterBtnClick() {
        Activity mActivity = getActivity();
        if (checkInputs() && mActivity != null) {
            checkDuplicateUser(emailInput.getText().toString(), mActivity);
        }
    }

    private MaterialDialog initRegisterDialog() {
        MaterialDialog.Builder mdb = new MaterialDialog.Builder(mContext)
                .title("Регистрация")
                .content("Обрабатываем данные...")
                .progress(true, 0);
        return mdb.build();
    }

    private void uploadProfileImage(final String userId, final Activity activity) {
        StorageReference mStorageReference = FirebaseStorage.getInstance().getReference();
        StorageReference profileImageReference = mStorageReference.child(String.format("users_images/%s.jpg", userId));
        profileImageReference.putFile(mProfileImage).addOnSuccessListener(activity, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mRegisterDialog.dismiss();
                mContext.startActivity(new Intent(mContext, MainActivity.class));
                if (activity != null)
                    activity.finish();
            }
        }).addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mRegisterDialog.dismiss();
                Toast.makeText(mContext, "Не удалось загрузить изображение", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createUser(final Activity activity) {
        ECHash mEcHash = new ECHash();
        mEcHash.calculate(passwordInput.getText().toString(), ECHashAlgorithms.MD5, new ECResultListener() {
            @Override
            public void onProgress(int i, long l, long l1) {

            }

            @Override
            public <T> void onSuccess(T t) {
                Map<String, Object> user = new HashMap<>();
                user.put("email", emailInput.getText().toString());
                user.put("fullname", fullnameInput.getText().toString());
                user.put("password", ((String) t).toLowerCase());
                user.put("openLessons", 1);

                mDatabase.collection("users")
                        .add(user).addOnSuccessListener(activity, new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        String userId = documentReference.getId();
                        new StorageController(mContext).saveSession(userId);
                        if (mProfileImage != null) {
                            uploadProfileImage(userId, activity);
                        } else {
                            mRegisterDialog.dismiss();
                            mContext.startActivity(new Intent(mContext, MainActivity.class));
                            if (activity != null)
                                activity.finish();
                        }

                    }
                }).addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mRegisterDialog.dismiss();
                        Toast.makeText(mContext, "Ошибка регистрации", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onFailure(String s, Exception e) {

            }
        });
    }

    @OnClick(R.id.edit_profile_photo_fab)
    void onEditFabClick() {
        Intent mImageSelectIntent = new Intent();
        mImageSelectIntent.setType("image/*");
        mImageSelectIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(mImageSelectIntent, "Выберите изображение"), 1);
    }

    private void loadEmptyUserAvatar() {
        loadImage(mContext.getResources().getDrawable(R.drawable.empty_user_avatar));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Uri mUri = data.getData();
            if (mUri != null) {
                mProfileImage = mUri;
                loadImage(mUri);
            } else {
                loadEmptyUserAvatar();
            }
        }
    }
}
