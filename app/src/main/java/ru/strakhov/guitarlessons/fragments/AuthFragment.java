package ru.strakhov.guitarlessons.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.pvryan.easycrypt.ECResultListener;
import com.pvryan.easycrypt.hash.ECHash;
import com.pvryan.easycrypt.hash.ECHashAlgorithms;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.strakhov.guitarlessons.R;
import ru.strakhov.guitarlessons.activities.AuthActivity;
import ru.strakhov.guitarlessons.activities.MainActivity;
import ru.strakhov.guitarlessons.controllers.StorageController;

public class AuthFragment extends Fragment {

    public static final String TAG = "AuthFragment";
    public static final String AUTH = "auth";
    public static final String STOP = "stop";

    @BindView(R.id.email_input)
    MaterialEditText mEmailInput;
    @BindView(R.id.password_input)
    MaterialEditText mPasswordInput;
    private FirebaseFirestore mDatabase;
    private BroadcastReceiver mAuthBroadcastReceiver;
    private StorageController mStorageController;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_auth, container, false);
        mContext = getContext();
        if (mContext != null)
            mStorageController = new StorageController(mContext);
        if (isAuth()) {
            Activity mActivity = getActivity();
            startActivity(new Intent(mContext, MainActivity.class));
            if (mActivity != null)
                mActivity.finish();
        }
        ButterKnife.bind(this, mView);
        setupDatabase();
        if (mContext != null)
            mContext.sendBroadcast(new Intent().setAction(AuthActivity.INVALIDATE));
        return mView;
    }

    private boolean isAuth() {
        return mStorageController.checkIsActiveSession();
    }

    private void setupDatabase() {
        FirebaseFirestoreSettings mFirebaseFirestoreSettings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        mDatabase = FirebaseFirestore.getInstance();
        mDatabase.setFirestoreSettings(mFirebaseFirestoreSettings);
    }

    private boolean checkInputs() {
        boolean check = true;
        String error_message = mContext.getResources().getString(R.string.error_message_input);
        if (mEmailInput.getText().toString().trim().isEmpty()) {
            check = false;
            mEmailInput.setError(error_message);
        }
        if (mPasswordInput.getText().toString().trim().isEmpty()) {
            check = false;
            mPasswordInput.setError(error_message);
        }
        return check;
    }

    private MaterialDialog initDialog() {
        String title, content;
        title = mContext.getResources().getString(R.string.auth_dialog_title);
        content = mContext.getResources().getString(R.string.auth_dialog_content);
        MaterialDialog.Builder mdb = new MaterialDialog.Builder(mContext)
                .title(title)
                .content(content)
                .progress(true, 0);
        return mdb.build();
    }

    private void startListener() {
        final IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(AUTH);
        mIntentFilter.addAction(STOP);
        mAuthBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null) {
                    switch (intent.getAction()) {
                        case AUTH:
                            if (getActivity() != null)
                                auth(getActivity());
                            break;
                        case STOP:
                            mContext.unregisterReceiver(mAuthBroadcastReceiver);
                            break;
                    }
                }
            }
        };
        mContext.registerReceiver(mAuthBroadcastReceiver, mIntentFilter);
    }

    private void auth(final Activity activity) {
        final String emailValue, passwordValue, authError;
        emailValue = mEmailInput.getText().toString();
        passwordValue = mPasswordInput.getText().toString();
        authError = mContext.getResources().getString(R.string.auth_error_message);
        if (checkInputs()) {
            final MaterialDialog authDialog = initDialog();
            authDialog.show();
            ECHash mEcHash = new ECHash();
            mEcHash.calculate(passwordValue, ECHashAlgorithms.MD5, new ECResultListener() {
                @Override
                public void onProgress(int i, long l, long l1) {

                }

                @Override
                public <T> void onSuccess(T t) {
                    CollectionReference usersCollectionReference = mDatabase.collection("users");
                    String encryptPassword = ((String) t).toLowerCase();
                    usersCollectionReference.whereEqualTo("email", emailValue)
                            .whereEqualTo("password", encryptPassword)
                            .get().addOnSuccessListener(activity, new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot documentSnapshots) {
                            int usersFouncCount = documentSnapshots.size();
                            if (usersFouncCount > 0) {
                                authDialog.dismiss();
                                String userId = documentSnapshots.getDocuments().get(0).getId();
                                mStorageController.saveSession(userId);
                                startActivity(new Intent(mContext, MainActivity.class));
                                activity.finish();
                            } else {
                                authDialog.dismiss();
                                Toast.makeText(mContext, authError, Toast.LENGTH_LONG).show();
                            }
                        }
                    }).addOnFailureListener(activity, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
//                            Log.e("YAY!", "check auth error");
                        }
                    });
                }

                @Override
                public void onFailure(String s, Exception e) {

                }
            });
        }
    }

    @OnClick(R.id.register_btn)
    void onRegisterBtnClick() {
        Intent mRegisterIntent = new Intent();
        mRegisterIntent.setAction(AuthActivity.OPEN_REGISTER);
        mContext.sendBroadcast(mRegisterIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        startListener();
    }
}
