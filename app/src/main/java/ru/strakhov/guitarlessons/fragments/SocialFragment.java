package ru.strakhov.guitarlessons.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.strakhov.guitarlessons.GlideApp;
import ru.strakhov.guitarlessons.R;

public class SocialFragment extends Fragment {

    @BindView(R.id.author_image)
    AppCompatImageView mAuthorImage;

    public static final String TAG = "SocialFragment";
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getContext();
        View mView = LayoutInflater.from(mContext).inflate(R.layout.layout_social, container, false);
        ButterKnife.bind(this, mView);
        setupUI();
        return mView;
    }

    private void onSocialClick(String social) {
        Intent mSocialIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(social));
        if (social.contains("vk") && mContext.getPackageManager().getLaunchIntentForPackage(mContext.getResources().getString(R.string.VK_APP_PACKAGE_ID)) != null)
            mSocialIntent.setPackage(mContext.getResources().getString(R.string.VK_APP_PACKAGE_ID));
        if (social.contains("instagram") && mContext.getPackageManager().getLaunchIntentForPackage(mContext.getResources().getString(R.string.INSTAGRAM_PACKAGE_ID)) != null)
            mSocialIntent.setPackage(mContext.getResources().getString(R.string.INSTAGRAM_PACKAGE_ID));
        if (social.contains("youtube") && mContext.getPackageManager().getLaunchIntentForPackage(mContext.getResources().getString(R.string.YOUTUBE_PACKAGE_ID)) != null)
            mSocialIntent.setPackage(mContext.getResources().getString(R.string.YOUTUBE_PACKAGE_ID));
        mContext.startActivity(mSocialIntent);
    }

    @OnClick(R.id.instagram)
    public void onInstagramClick() {
        onSocialClick(mContext.getResources().getString(R.string.instagram));
    }

    @OnClick(R.id.vk)
    public void onVkClick() {
        onSocialClick(mContext.getResources().getString(R.string.vk));
    }

    @OnClick(R.id.youtube)
    public void onYouTubeClick() {
        onSocialClick(mContext.getResources().getString(R.string.youtube));
    }

    private void setupUI() {
        GlideApp.with(mContext)
                .load(R.drawable.author)
                .centerCrop()
                .circleCrop()
                .skipMemoryCache(true)
                .into(mAuthorImage);
    }
}
